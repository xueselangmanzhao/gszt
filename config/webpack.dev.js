const { merge } = require("webpack-merge");
const baseConfig = require("./webpack.base.js");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = merge(baseConfig, {
  mode: "development",
  target: "web",
  plugins: [
    new FriendlyErrorsWebpackPlugin(),
    // 注意如果不声明文件扩展名，eslint默认只会检查js结尾的文件
    new ESLintPlugin({ extensions: ["js", "vue"] }),
  ],
  devServer: {
    hot: true, //启用热模块替换
    open: true, //打开默认浏览器
  },
});
