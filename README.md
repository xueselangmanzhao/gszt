## 项目执行
1. 执行 npx lint-staged 就能手动在暂存区运行 eslint+prettier 做代码风格校验

## 执行测试
DEBUG_MODE=1 npx jest  打开electron
npx jest 


## 安装git husky
"prepare": "husky install",

## 打包分析
"analyze": "webpack-bundle-analyzer --port 3000 ./dist/stats.json"