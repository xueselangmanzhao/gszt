module.exports = {
  "src/**/*.{js,vue}": [
    "eslint --fix --ext .js,.vue",
    "prettier --write",
  ],
};